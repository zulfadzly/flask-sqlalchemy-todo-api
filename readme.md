Development setup

1. Update the env-dist-development parameters
2. Rename env-dist-development to .env
3. Build todo-api docker container
docker build -t todoapi:latest .

4. Start docker container

docker run -d \
--name todo-api \
-p 5002:5002 \
--restart=always \
-e API_ENV=dev \
-e DB_USER=root \
-e DB_PASSWORD=testlab2 \
-e DB_HOST=172.17.0.1 \
-e DB=todo \
-v $(pwd)/.env:/todo/app/.env \
-v $(pwd):/todo \
--link localMysql:localMysql \
todoapi:latest \
python3 -u manage.py run

5. Install todo db

a) migration
docker exec -it todo-api python3 manage.py db init

docker exec -it todo-api python3 manage.py db migrate --message 'initial database migration'

docker exec -it todo-api python3 manage.py db upgrade

6.Browse http://localhost:5002/api/1/


Deployment

1. Rename env-dist-deployment to .env

2. Run docker compose
docker-compose up -d --remove-orphans

3. Install todo db
docker exec -it todo-api python3 manage.py db init

docker exec -it todo-api python3 manage.py db migrate --message 'initial database migration'

docker exec -it todo-api python3 manage.py db upgrade

4.Browse http://localhost:5002/api/1/

5.Stop docker container
docker-compose down --remove-orphans


