FROM centos:8

MAINTAINER Zulfadzly <mzulfadzly@yahoo.com>

ADD . /todo

RUN yum -y update && \
    yum -y install \
	telnet \
	python36 \
	python3-devel \
	gcc-c++ \
	mysql-devel-8.0.17-3.module_el8.0.0+181+899d6349.x86_64 \
    wget && \
    curl https://bootstrap.pypa.io/get-pip.py | python3.6 && \
    yes | pip3 install -r /todo/requirements.txt && \
    yum remove -y --skip-broken --nobest \
    python3-devel \
    gcc-c++ && \
    yum clean all && \
    rm -rf /var/cache/yum

WORKDIR /todo

EXPOSE 5002

CMD ["python3", "-u", "manage.py", "run"]