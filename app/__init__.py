import os
from flask_restx import Api
from flask import Blueprint
from dotenv import load_dotenv
from .main.controller.todo_controller import api as todo_ns

load_dotenv()

blueprint = Blueprint('api', __name__)

authorizations = {
    'apiKey': {
        'type': 'apiKey',
        'in': 'header',
        'name': 'X-API-KEY'
    }
}


if os.getenv('API_ENV') == 'prod' or os.getenv('API_ENV') == 'staging':
    api = Api(blueprint,
              title='Todo',
              version='1.0',
              description='TODO list API',
              authorizations=authorizations,
              doc=False
              )
else:
    api = Api(blueprint,
              title='Todo',
              version='1.0',
              description='TODO list API',
              authorizations=authorizations,
              )


api.add_namespace(todo_ns, path='/todo')