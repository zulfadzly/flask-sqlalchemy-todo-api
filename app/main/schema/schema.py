from .. import db, ma
from app.main.model.todo import Todo


class TodoSchema(ma.Schema):
    class Meta:
        model = Todo,
        fields = (
            'id',
            'task',
            'show_status',
            'create_date',
            'update_date',
        )

new_todo_schema = TodoSchema()