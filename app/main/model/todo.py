from .. import db
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import text

Base = declarative_base()

class Todo(db.Model, Base):

    """ Todo Model for storing todo list """
    __tablename__ = "todo"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    task = db.Column(db.String(255), nullable=False)
    show_status = db.Column(db.String(20), nullable=False, server_default="active")
    create_date = db.Column(db.DateTime, nullable=False)
    update_date = db.Column(db.DateTime, nullable=True)

    def __init__(self, task ,create_date):
        self.task = task
        self.create_date = create_date