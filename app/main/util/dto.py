from flask_restx import Namespace, fields


class TodoDto:

    api = Namespace('todo', description='todo related operations')

    todo = api.model('todo', {
        'id': fields.Integer,
        'task': fields.String,
        'show_status': fields.String,
    })

    createTodo = api.model('createTodo', {
        'task': fields.String
    })

    updateTodo = api.model('updateTodo', {
        'task': fields.String
    })

    updateTodoStatus = api.model('updateTodoStatus', {
        'show_status': fields.String
    })