import logging, sys
from flask import request
from flask_restx import Resource,  Namespace, fields, abort
from functools import wraps
from ..config import token

from ..util.dto import TodoDto
from ..service.todo_service import get_all_todo, save_new_todo ,delete_todo ,get_todo ,update_todo ,update_todo_status

api = TodoDto.api
_todo = TodoDto.todo
_createTodo = TodoDto.createTodo
_updateTodo = TodoDto.updateTodo
_updateTodoStatus = TodoDto.updateTodoStatus


logging_formatter = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(filename='/todo/app/logs/app.log', level=logging.DEBUG, format=logging_formatter)
console = logging.StreamHandler()
console.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
console.setFormatter(formatter)
logging.getLogger('customerFeedback').addHandler(console)
logger = logging.getLogger(__name__)

def token_required(f):
    @wraps(f)
    def decorated(*args, **kwagrs):

        request_token = None

        if 'X-API-KEY' in request.headers:
            request_token = request.headers['X-API-KEY']

        if not token:
            return {'message': 'Token is missing.'}, 401

        if request_token != token:
            return {'message': 'wrong token passed.'}, 401

        return f(*args, ** kwagrs)

    return decorated


@api.route('/')
class TodoList(Resource):
    @api.doc(security='apiKey')
    @token_required
    @api.doc('all_todo_list')
    @api.marshal_list_with(_todo, envelope='data')
    def get(self):
        """ List all todo """
        r = request
        logger.info('%s %s', 'List all todo', r)
        return get_all_todo()

    @api.response(200, 'todo successfully created.')
    @api.doc(security='apiKey')
    @token_required
    @api.doc('create_new_todo')
    @api.expect(_createTodo, validate=True)
    def post(self):
        """ Creates a new todo """
        data = request.json
        logger.info('%s %s', 'Creates a new todo', data)
        return save_new_todo(data=data)


@api.route('/<int:id>')
@api.param('id', 'The todo identifier')
@api.response(200, 'Success')
@api.response(404, 'Failed')
class GetTodo(Resource):
    @api.doc(security='apiKey')
    @token_required
    @api.doc('delete_todo')
    def delete(self, id):
        """ Delete todo """
        logger.info('%s %s', 'Delete todo #', id)
        return delete_todo(id)

    @api.doc(security='apiKey')
    @token_required
    @api.doc('get_todo')
    @api.marshal_list_with(_todo,  envelope='data')
    def get(self, id):
        """ Get todo """
        logger.info('%s %s', 'Get todo #', id)
        return get_todo(id)

    @api.doc(security='apiKey')
    @token_required
    @api.doc('update_todo')
    @api.expect(_updateTodo, validate=True)
    def put(self, id):
        """ Update todo """
        data = request.json
        logger.info('%s %s %s', 'Update todo', id, data)
        return update_todo(id, data=data)

    @api.doc(security='apiKey')
    @token_required
    @api.doc('update_todo_status')
    @api.expect(_updateTodoStatus, validate=True)
    def patch(self, id):
        """ Update todo status """
        data = request.json
        logger.info('%s %s %s', 'Update todo status #', id, data)
        return update_todo_status(id, data=data)