import os

basedir = os.path.abspath(os.path.dirname(__file__))

class Config:

    DEBUG = True
    SECRET_KEY = os.getenv('SECRET_KEY', 'rain in sp@in falls mainly on the Plain')
    TOKEN = '$NEIZ%pH<a"GQ4{*{Bw&LB+697_@^8'

class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SQLALCHEMY_ECHO = True
    SQLALCHEMY_DATABASE_URI = 'mysql://' + os.getenv('DB_USER') + ':' + os.getenv('DB_PASSWORD') + '@' + \
                              os.getenv('DB_HOST') + '/' + os.getenv('DB')

class TestingConfig(Config):
    DEBUG = True
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'todo.db')
    PRESERVE_CONTEXT_ON_EXCEPTION = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_ECHO = True

class QAConfig(Config):
    DEBUG = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SQLALCHEMY_ECHO = True
    SQLALCHEMY_DATABASE_URI = 'mysql://' + os.getenv('DB_USER') + ':' + os.getenv('DB_PASSWORD') + '@' + \
                              os.getenv('DB_HOST') + '/' + os.getenv('DB')

class StagingConfig(Config):
    DEBUG = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SQLALCHEMY_ECHO = True
    SQLALCHEMY_DATABASE_URI = 'mysql://' + os.getenv('DB_USER') + ':' + os.getenv('DB_PASSWORD') + '@' + \
                              os.getenv('DB_HOST') + '/' + os.getenv('DB')

class ProductionConfig(Config):
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = 'mysql://' + os.getenv('DB_USER') + ':' + os.getenv('DB_PASSWORD') + '@' + \
                              os.getenv('DB_HOST') + '/' + os.getenv('DB')


config_by_name = dict(
    dev=DevelopmentConfig,
    test=TestingConfig,
    staging=StagingConfig,
    qa=QAConfig,
    prod=ProductionConfig
)
token = Config.TOKEN
