import uuid
import datetime
from app.main import db
from app.main.model.todo import Todo
from app.main.schema.schema import new_todo_schema
from sqlalchemy.exc import SQLAlchemyError
import logging
import sys

logging_formatter = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(filename='/todo/app/logs/app.log', level=logging.DEBUG, format=logging_formatter)
console = logging.StreamHandler()
console.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
console.setFormatter(formatter)
logging.getLogger('customerFeedback').addHandler(console)
logger = logging.getLogger(__name__)

def get_all_todo():
    return db.session.query(Todo).all()

def save_new_todo(data):

    try:
        new_todo = Todo(
            task=data['task'],
            create_date=datetime.datetime.utcnow()
        )
        db.session.add(new_todo)
        db.session.commit()
        new_todo_data = new_todo_schema.dump(new_todo)
        response_object = {
            'message': 'Todo successfully created.',
            'status': True,
            'data': new_todo_data,
        }
        logger.info('%s %s', 'successfully created todo #', new_todo.id)
        return response_object, 200
    except SQLAlchemyError as e:
        logger.error('%s %s', 'failed to create todo error', e)
        db.session.rollback()
        db.session.flush()
        response_object = {
            'message': 'Failed to create todo',
            'status': False,
        }
        return response_object, 404


def delete_todo(todo_id):

    try:
        todo = db.session.query(Todo).filter(Todo.id == todo_id).first()
        db.session.delete(todo)
        db.session.commit()
        response_object = {
            'message': 'Todo deleted successfully',
            'status': True,
        }
        return response_object, 200
    except SQLAlchemyError as e:
        logger.error('%s %s', 'failed to delete todo error', e)
        db.session.rollback()
        db.session.flush()
        response_object = {
            'message': 'Failed to delete todo',
            'status': False,
        }
        return response_object, 404


def get_todo(todo_id):
    todo = Todo.query.filter_by(id=todo_id).first()
    if todo is None:
        return False
    else:
        return todo


def update_todo(todo_id, data):
    todo_exist = len(Todo.query.filter_by(id=todo_id).all())
    if todo_exist == 1:
        todo = Todo.query.filter_by(id=todo_id).first()
        todo.task = data['task']
        todo.update_date = datetime.datetime.utcnow()
        db.session.commit()
        update_todo_data = new_todo_schema.dump(todo)
        response_object = {
            'message': 'Todo updated successfully',
            'status': True,
            'data': update_todo_data,
        }
        logger.info('%s %s', 'successfully updated todo #', todo.id)
        return response_object, 200
    else:
        response_object = {
            'message': 'Failed to update todo',
            'status': False,
        }
        logger.error('%s %s', 'failed to updated todo #', todo_id)
        return response_object, 404


def update_todo_status(todo_id, data):
    todo_exist = len(Todo.query.filter_by(id=todo_id).all())
    if todo_exist == 1:
        todo = Todo.query.filter_by(id=todo_id).first()
        todo.update_date = datetime.datetime.utcnow()
        todo.show_status = data['show_status']
        db.session.commit()
        response_object = {
            'message': 'Todo status updated successfully',
            'status': True,
        }
        logger.info('%s %s', 'successfully updated todo status #', todo.id)
        return response_object, 200
    else:
        response_object = {
            'message': 'Failed to update todo status',
            'status': False,
        }
        logger.error('%s %s', 'failed to updated todo status #', todo_id)
        return response_object, 404